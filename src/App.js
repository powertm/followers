import React from 'react';
import './App.css';

class App extends React.Component {


  constructor(props) {
    super(props);

    this.statuses = { 'Following': 'Follow', 'Follow': 'Following' };

    this.state = {
      users: [],
      user: {}
    }
  }


  componentDidMount() {

    if ( this.userLoggedIn() ) {
      this.fetchUsers().then( users => this.setState( { users: users, user: users[0] } ) );
    }        
  }


  fetchUsers() {

    const users = fetch( 'http://localhost:8888/users/' )
    .then( res => res.json() )
    .then( res => { return res } );

    return users;
  }

  addFollower(userToFolloweId, followerId) {
    fetch( 'http://localhost:8888/add-follower/?to-follow-id=' + userToFolloweId + '&follower-id=' + followerId )
    .then( res => res.json() )
    .then( res => { return res } )
  }

  removeFollower(userFollowList, userToUnfollow, currentUserId) {
    fetch( 'http://localhost:8888/remove-follower/?follow-list=' + userFollowList + '&unfollow-id=' + userToUnfollow + '&user-id=' + currentUserId )
    .then( res => res.json() )
    .then( res => { return res } )
  }

  userLoggedIn() {
    // Server cookie check (would normally use this way
    // but not enough time to figure out why JSON.parse() doesn't work correctly):
    // fetch( 'http://localhost:8888/check-login/', { credentials: 'include', mode: 'no-cors' } )
    // .then( res => res.json() )
    // .then( res => {
    //   if ( res.error === 'undefined' ) {
    //   }
    // })

    let cookies = document.cookie;

    if ( cookies === '' ) {
      return false;
    }

    let loggedIn = false;

    cookies = cookies.split('; ');

    cookies.forEach( ( cookie ) => {

      let tempCookie = cookie.split('=');

      if ( typeof tempCookie !== 'undefined' && tempCookie[0] === 'user_id' && typeof tempCookie[1] !== 'undefined'  ) {
        loggedIn = true;
      }
    })

    return loggedIn;
  }

  changeStatus(userIndex, status, targetUser, currentUser) {

    const newUsers = this.state.users.concat();

    if ( status === 'Follow' ) {
      this.addFollower( targetUser, currentUser );
      newUsers[userIndex].followers = newUsers[userIndex].followers + 1;
      newUsers[0].following = newUsers[0].following + ',' + targetUser;
    }

    else if ( status === 'Following' ) {

      this.removeFollower( newUsers[0].following, targetUser, currentUser );
      newUsers[userIndex].followers = newUsers[userIndex].followers - 1;

      newUsers[0].following = newUsers[0].following.split(',');
      const unfollowIdIndex = newUsers[0].following.indexOf( String(targetUser) );

      if ( unfollowIdIndex  > -1 ) {
        newUsers[0].following.splice( unfollowIdIndex, 1 );
      }

      newUsers[0].following = newUsers[0].following.join(',');

    }

    

    this.setState({
      users: newUsers,
    })
  }


  render() {

    const userRows = this.state.users.map( (user, i) => {

      const currentUser = this.state.user;
      const status = currentUser.following.split(',').includes(String(user.uid)) ? 'Following' : 'Follow';

      return (
        <UserRow 
          key={ user.uid }
          name={ user.name }
          group={ user.groupName }
          followers={ user.followers }
          status={ status }
          id={ user.uid }
          onClick={ () => this.changeStatus( i, status, user.uid, currentUser.uid ) }
        />
      )

    });


    return (
      <div className="App">
        <UsersList loggedIn={() => this.userLoggedIn()} user={ this.state.user }>{ userRows }</UsersList>
      </div>
    );
  }
}

function UsersList(props) {

  function logIn(e) {

    e.preventDefault();

    fetch( 'http://localhost:8888/login/', { credentials: 'include', mode: 'no-cors' } )
    .then( res => {
      window.location.reload();
    })
  }

  if ( !props.loggedIn() ) {
    return ( <div>Must be signed in. <a className="link" href="/" onClick={(e) => logIn(e)}>Log in</a></div> )
  }

  return (
    <div id="users-list">
      <h1>Welcome, {props.user.name}</h1>
      <span className="user-name title">Name</span>
      <span className="user-group title">Group</span>
      <span className="user-followers title">Followers</span>
      <div className="sep"/>
      { props.children }
    </div>
  )
}

function UserRow(props) {

  function handleClick(uid, status) {
    props.onClick(uid, status)
  }


  return (
    <div className="user-row">

      <span className="user-name">{ props.name }</span>
      <span className="user-group">{ props.group }</span>
      <span className="user-followers">{ props.followers }</span>

      <FollowButton
        onClick={ () => handleClick(props.id, props.status) }
        className={ props.status }
        userId={ props.id }
        status={ props.status }
      />
    </div>
  )
}


function FollowButton(props) {

  const hoverOptions = { 'Following': 'Unfollow', 'Unfollow': 'Following' };

  function handleMouseEnter(e) {

    const buttonText = e.target.textContent;

    if ( buttonText === 'Follow' ) {
      return false;
    }

    e.target.textContent = hoverOptions[buttonText];
    e.target.classList.add( hoverOptions[buttonText] );
    
  }

  function handleMouseLeave(e) {
    const buttonText = e.target.textContent;

    if ( buttonText === 'Follow' || buttonText === 'Following' ) {
      return false;
    }

    e.target.classList.remove( 'Unfollow' );
    e.target.textContent = hoverOptions[buttonText];

  }

  function handleClick(status) {
    props.onClick(status);
  }


  return (
    <button
      className={ props.className }
      onClick={ () => handleClick(props.userId, props.status) }
      onMouseLeave={ (e) => handleMouseLeave(e) }
      onMouseEnter={ (e) => handleMouseEnter(e) }
      >{props.status}
    </button>
    )
}

export default App;
