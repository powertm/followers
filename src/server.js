const http    = require( 'http' );
const mysql   = require( 'mysql' );
const urlUtil = require( 'url' );

const db = mysql.createConnection({
  host: '46.101.207.197',
  user: 'followers',
  password: 'P:AK]xw"+!2]#RFc',
  database: 'followers'
});

const sendParamsError = (res) => {
  res.end( JSON.stringify({ error: 'Critical params not supplied.' }) );
}

http.createServer( (req,res) => {

  res.writeHeader(200, {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*'});

  const response = '';
  const url = urlUtil.parse( req.url, true );


  /* GET /users/ */
  if ( url.pathname === '/users/' ) {

    db.query('SELECT uid, users.name, followers, following, g.name AS groupName \
              FROM users \
              LEFT JOIN groups AS g ON g.gid = group_id',
              ( err, result, fields ) => {

      if ( err ) {
        throw err
      }

      const users = JSON.stringify(result);
      res.end( users );
      
    })
  }

  /* GET /add-follower/ */
  else if ( url.pathname === '/add-follower/' ) {

    let userToFollow = url.query['to-follow-id'],
        follower = url.query['follower-id'];

    if ( userToFollow === undefined || follower === undefined ) {
      sendParamsError(res);
    } 

    // Casting to a number when defining a variable (above) could turn
    // 'undefined' to a number as well, therefore casting to number after validation: 
    else {
      userToFollow = Number( userToFollow );
      follower = Number( follower );
    }


    // Add 1 follower to user to be followed total followers:
    db.query( 'UPDATE users SET followers = followers + 1 \
               WHERE uid = ' + userToFollow,
               (err, result, fields) => {

      if ( err ) {
        res.end( JSON.stringify({ error: 'Could not add follower to user id ' + userToFollow }) );
      }

    });


    // Append to users our current user is following:
    db.query( 'UPDATE users SET following = CONCAT(following, ",' + userToFollow + '") \
               WHERE uid = ' + follower,
               (err, result, fields) => {

      if ( err ) {
        res.end( JSON.stringify({ error: 'Could not add following to ' + follower }) );
      }
    });


    res.end( JSON.stringify({}) );
  }

  /* GET /remove-follower/ */
  else if ( url.pathname === '/remove-follower/' ) {

    let userFollowList  = url.query['follow-list'],
        userToUnfollow  = url.query['unfollow-id'],
        userId          = url.query['user-id'];

    if ( userFollowList === 'undefined' || userToUnfollow === 'undefined' || userId === 'undefined' ) {
      sendParamsError(res)
    }

    // Remove 1 follower from user to be unfollowed:
    db.query( 'UPDATE users SET followers = followers - 1 \
               WHERE uid = ' + userToUnfollow,
               (err, result, fields) => {

      if ( err ) {
        res.end( JSON.stringify({ error: 'Could not remove follower from user id ' + userToUnfollow }) );
      }
    });

    // Remove user to unfollow from current user's followings list:
    userFollowList = userFollowList.split(',');

    const unfollowIdIndex = userFollowList.indexOf(userToUnfollow);

    if ( unfollowIdIndex > -1 ) {
      userFollowList.splice( unfollowIdIndex, 1 )
    }

    userFollowList = userFollowList.join(',');

    db.query( 'UPDATE users SET following = "' + userFollowList + '" \
               WHERE uid = ' + userId,
               (err, result, fields) => {

      if ( err ) {
        res.end( JSON.stringify({ error: 'Could remove following from ' + userId }) );
      }
    });


    res.end( JSON.stringify({}) )
  }

  else if ( url.pathname === '/login/') {

    res.writeHeader(200, {
      'Access-Control-Allow-Origin': '*',
      'Set-Cookie': 'user_id=6; Max-Age=2592000; Path=/' // In real life I would set it to a relevant login ID from the DB
    });

    res.end( JSON.stringify({}) );
  }

  else if ( url.pathname === '/check-login/') {

    let cookies = req.headers.cookie;

    if ( typeof cookies === 'undefined' ) {
      res.end( JSON.stringify( {error: 'User not logged in.'} ) );
      return false;
    }

    cookies = cookies.split('; ');

    cookies.forEach( ( cookie ) => {
      let tempCookie = cookie.split('=');


      if ( typeof tempCookie !== 'undefined' && tempCookie[0] === 'user_id' && typeof tempCookie[1] !== 'undefined'  ) {
        res.end( JSON.stringify({}) )
      }
    })

    res.end( JSON.stringify( {error: 'User not logged in.'} ) );
  }

  /* Default catch-all */
  else {
    res.end( response );
  }
	
	
}).listen(8888);

console.log( 'Server running on http://localhost:8888' );